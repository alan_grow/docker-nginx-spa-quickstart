Docker and NGINX Setup for SPA
==============================

This setup was used on a VM running Debian 10.

Getting Docker
--------------

I followed instructions on the [official
docs](https://docs.docker.com/install/linux/docker-ce/debian/) to
install Docker.

```bash
# Should go download a container and run it, which will display a message
sudo docker run hello-world
```

If you don't want to keep typing `sudo` when doing stuff with docker, you can
follow the official [post-installation steps for
Linux](https://docs.docker.com/install/linux/linux-postinstall/).

Preparing a Single Page Application
-----------------------------------

Assuming you have `node` install (I have 13.x), you should be able to
get a a quick working Single Page Application (SPA) up and running
using Facebook's `create-react-app`.

```bash
npx create-react-app my-app
cd my-app
npm start
```

After doing this you can try starting up your app and seeing how it
should look. Then build it using `npm run build` (see `package.json`
in the SPA for details on the magic of what is actually going on when
you run this script.

Getting NGINX
-------------

Now we are going to run NGINX (our web server) on Docker. Here, I
consult the official docs on Docker's [hub entry for
nginx](https://hub.docker.com/_/nginx) and "[Deploying NGINX and NGINX
Plus on
Docker](https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-docker/) on the official NGINX docs.

Note that you don't need to install NGINX outside of Docker
containers, though it may be helpful to do so to get used to how it
works.

Building, Containerizing, and Deploying
---------------------------------------

From the directory in which you have a `Dockerfile`, you can run...

```bash
docker build --tag name_of_my_app:1.0 .
```

...where you can change 1.0 to correspond to whatever version number
you would like. Since this is just like running any other command line
program you can use your shell's features like variables and the like
which is helpful in automating workflows.

```bash
docker run --publish 8000:8080 --detach --name foo name_of_my_app:1.0
```

Navigating to localhost:8000 in a browser, you should be able to see
your SPA.

To delete your app, do:

```bash
docker rm --force foo
```

More details on the commands above can be found in Docker's [Getting
Started](name_of_my_app:1.0) documents.



A handful of opinions
---------------------

- I don't like to register for stuff; poo! Well, I had to register for
  docker hub.

Links and Resources
-------------------

- https://github.com/tamland/spa-server 
  - Referenced `nginx.conf` and `Dockerfile`; used similar file structure
