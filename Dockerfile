FROM nginx:alpine

EXPOSE 80

# COPY nginx.conf /etc/nginx/conf.d/default.conf # Default
COPY start.sh /start.sh
RUN chmod +x /start.sh

# Copy the rest of your app's source code from your host to your image filesystem.
COPY index.html /var/www/html/
# Maybe try using RUN to get create-react-app going.
COPY nginx.conf /etc/nginx/nginx.conf

CMD /start.sh


